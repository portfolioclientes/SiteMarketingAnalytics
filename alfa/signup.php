<?php 

	// Conexao Banco de dados
	require_once('lib/php/conexaoDb.php');

    // Classes para usuario, conexao e recuperacao de senha
    require_once('lib/php/user.php');
	require_once('lib/php/phpmailer/mail.php');
	
	// Instancia de novo usuario
    $user = new User($db);

	// Se estiver conectado redirecionar para área de membros
	if( $user->is_logged_in() ){ 
		header('Location: dashboard.php'); exit(); 
	}

	// Se o formulário foi submetido processá-lo
	if(isset($_POST['submit'])){

		// Se nao informou o nome do usuario
		if (!isset($_POST['username'])){
			$error[] = "Nome do usuário não informado!";
		} 

		// Se nao informou e-mail
		if (!isset($_POST['email'])){
			$error[]    = "E-mail não informado!";
		} 

		// Se nao inforou a senha
		if (!isset($_POST['password'])){
			$error[] = "Senha não informada!";
		} 

		// Guarda o nome do usuario
		$username = $_POST['username'];

		// Validacao basica
		if(!$user->isValidUsername($username)){
			$error[] = 'Nome do usuário deve conter pelo menos três caracteres alfanuméricos!';
		} else {
			$stmt = $db->prepare('SELECT username FROM members WHERE username = :username LIMIT 1');
			$stmt->execute(array(':username' => $username));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			// Se usuario ja existe
			if(!empty($row['username'])){
				$error[] = 'Usuário já existe!';
			}
		}

		// Se a senha e curta
		if(strlen($_POST['password']) < 3){
			$error[] = 'Senha é muito curta.';
		}

		// Se a senha de confirmacao e curta
		if(strlen($_POST['passwordConfirm']) < 3){
			$error[] = 'Senha de confirmação é muito curta.';
		}

		// Se as senhas nao coincidem
		if($_POST['password'] != $_POST['passwordConfirm']){
			$error[] = 'As senhas não coincidem.';
		}

		// E-mail de validacao
		$email = htmlspecialchars_decode($_POST['email'], ENT_QUOTES);
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$error[] = 'Informe um endereço de e-mail válido!';
		} else {
			$stmt = $db->prepare('SELECT email FROM members WHERE email = :email');
			$stmt->execute(array(':email' => $email));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			if(!empty($row['email'])){
				$error[] = 'O email fornecido já está em uso!';
			}
		}

		// Se nao houve erros segue para e-mail de confirmacao
		if(!isset($error)){

			// Hash da senha
			$hashedpassword = $user->password_hash($_POST['password'], PASSWORD_BCRYPT);

			// Cria codigo de ativacao
			$activasion = md5(uniqid(rand(),true));

			try {
				// Prepara a query de insercao
				$stmt = $db->prepare('INSERT INTO members (username,password,email,active) VALUES (:username, :password, :email, :active)');

				$stmt->execute(array(
					':username' => $username,
					':password' => $hashedpassword,
					':email'    => $email,
					':active'   => $activasion
				));

				$id = $db->lastInsertId('memberID');

				// Envia e-mail
				$to = $_POST['email'];
				$subject = "Confirmação de registro";
				$body = "<p>Obrigado por se registrar.</p>
				<p>Para ativar a conta clique no link: <a href='".DIR."activate.php?x=$id&y=$activasion'>".DIR."activate.php?x=$id&y=$activasion</a></p>";
				$mail = new Mail();
				$mail->setFrom(SITEEMAIL);
				$mail->addAddress($to);
				$mail->subject($subject);
				$mail->body($body);
				$mail->send();

				// Retorna para pagina inicial
				header('Location: signup.php?action=joined');
				exit;
			// Tratamento de excecao
			} catch(PDOException $e) {
				$error[] = $e->getMessage();
			}
		}
	}

	// Titulo da pagina
	$title = 'Cadastro de usuários';

	// Template cabecalho
	require_once('header.php'); 
?>
<div class="content-wrap">
	<div class="container clearfix w-3xl">
		<form role="form" method="post" action="" autocomplete="off">
			<h2><i class="fa fa-lock"></i> Cadastro de usuário</h2>
			<p>Já possui cadastro? <a href='login.php'>Login</a></p>
			<?php
				// Se possui erros
				if(isset($error)){
					foreach($error as $error){
						echo '<p class="alert alert-danger">'.$error.'</p>';
					}
				}
				// Se houve sucesso na acao
				if(isset($_GET['action']) && $_GET['action'] == 'joined'){
					echo "<h4 class='alert alert-success'>Registro realizado com sucesso, verifique seu e-mail para ativar a conta.</h4>";
				}
			?>
            <div class="row">
				<div class="form-group col-md-12">
					<input type="text" name="username" id="username" class="form-control myInput" placeholder="Nome do usuário" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['username'], ENT_QUOTES); } ?>" tabindex="1">
				</div>
				<div class="form-group col-md-12">
					<input type="email" name="email" id="email" class="form-control myInput" placeholder="Endereço de e-mail" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['email'], ENT_QUOTES); } ?>" tabindex="2">
				</div>
				<div class="form-group col-md-12">
					<input type="password" name="password" id="password" class="form-control myInput" placeholder="Senha" tabindex="3">
				</div>
				<div class="form-group col-md-12">
					<input type="password" name="passwordConfirm" id="passwordConfirm" class="form-control myInput" placeholder="Confirme a senha" tabindex="4">
				</div>
				<div class="form-group col-md-12">
					<input type="submit" name="submit" value="Salvar" class="myBtn myBtn-rounded myBtn-dark m-0 mt-10" tabindex="5">
				</div>
			</div>
		</form>
	</div>
</div>
<?php
	// Template rodape
	require('footer.php');
?>