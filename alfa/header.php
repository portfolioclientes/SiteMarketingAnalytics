<!-- ============================================
============== Cadastro do usuario ==============
============================================= -->
<!DOCTYPE html>
<html class= "no-js"lang="pt-br">
<head>
    <!-- ============================================
    ================= Configuracoes html ============
    ============================================= -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <!-- ============================================
    ================= Estilo da Pagina ==============
    ============================================= -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,700italic|Raleway:300,400,500,600,700" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" type="text/css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"/>

    <link rel="stylesheet" href="lib/css/style.css" type="text/css" />

    <!-- ============================================
    ================== Titulo =======================
    ============================================= -->
    <title>Gestão de Portfólio</title>

</head>
<body>
    <!-- ============================================
    ================= Page Wrapper ==================
    ============================================= -->
    <div id="wrapper" class="clearfix animsition">

        <!-- ============================================
        ==================== Header =====================
        ============================================= -->
        <header id="header" class="transparent-header dark">
            <div id="header-wrap">
                <div class="container clearfix">
                    <div id="main-navbar-toggle"><i class="fa fa-bars"></i></div>

                    <!-- ============================================
                    =================== Branding ====================
                    ============================================= -->
                    <div id="branding">
                        <a href="index.html" class="brand-normal" data-light-logo="lib/images/logo.png"><img src="lib/images/logo-dark.png" alt=""></a>
                        <a href="index.html" class="brand-retina" data-light-logo="lib/images/logo@2x.png"><img src="lib/images/logo@2x-dark.png" alt=""></a>
                    </div><!-- #branding end -->

                    <!-- ============================================
                    ================= Main Navbar ===================
                    ============================================= -->
                    <nav id="main-navbar">
                        <ul>
                            <li><a href="dashboard.php">DASHBOARD</a></li>
                            <li><a href="login.php">LOGIN</a></li>
                            <li><a href="signup.php">SIGNUP</a></li>
                        </ul>
                    </nav><!-- #main-navbar end -->
                </div>
            </div>
        </header><!-- #header end -->