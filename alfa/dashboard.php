<!DOCTYPE html>
<html class="no-js" lang="pt-br">
<!-- =========================================================================
================= Testa se esta logado para acessar pagina ===================
========================================================================== -->
<?php  
	// Conexao Banco de dados
	require_once('lib/php/conexaoDb.php');

    // Classes para usuario, conexao e recuperacao de senha
    require_once('lib/php/user.php');

    // Instancia de novo usuario
    $user = new User($db);

	// Se não tiver efetuado o login, redirecione a página de login
    // if(!$user->is_logged_in()){ header('Location: login.php'); exit(); }
    
    if(isset($_SESSION['username'])){
        $usuario = $_SESSION['username'];
    } else {
        $usuario = "Ambiente Desenvolvimento";
    }

?>
<head>
    <title>Gestão Portfólio</title>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="icon" type="image/ico" href="lib/images/favicon.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1"/>

    <!-- ============================================
    ================= Stylesheets ===================
    ============================================= -->
    <link rel="stylesheet" href="lib/css/vendor/bootstrap.min.css"/>
    <link rel="stylesheet" href="lib/css/vendor/animate.css"/>
    <link rel="stylesheet" href="lib/css/vendor/font-awesome.min.css"/>
    <link rel="stylesheet" href="lib/css/triangulo.css"/>

    <link rel="stylesheet" href="lib/js/vendor/animsition/css/animsition.min.css"/>
    <link rel="stylesheet" href="lib/js/vendor/daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" href="lib/js/vendor/morris/morris.css"/>
    <link rel="stylesheet" href="lib/js/vendor/owl-carousel/owl.carousel.css"/>
    <link rel="stylesheet" href="lib/js/vendor/owl-carousel/owl.theme.css"/>
    <link rel="stylesheet" href="lib/js/vendor/rickshaw/rickshaw.min.css"/>
    <link rel="stylesheet" href="lib/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="lib/js/vendor/datatables/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="lib/js/vendor/datatables/datatables.bootstrap.min.css"/>
    <link rel="stylesheet" href="lib/js/vendor/chosen/chosen.css"/>
    <link rel="stylesheet" href="lib/js/vendor/summernote/summernote.css"/>
    <link rel="stylesheet" href="lib/js/vendor/footable/css/footable.core.min.css"/>

    <!-- project main css files -->
    <link rel="stylesheet" href="lib/css/main.css">
    <!--/ stylesheets -->

    <!-- ==========================================
    ================= Modernizr ===================
    =========================================== -->
    <script src="lib/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>

</head>
<body id="brots" class="appWrapper">

    <!-- ====================================================
    ================= Application Content ===================
    ===================================================== -->
    <div id="wrap" class="animsition">

        <!-- ===============================================
        ================= HEADER Content ===================
        ================================================ -->
        <section id="header">
            <header class="clearfix">
                <!-- Branding -->
                <div class="branding">
                    <a class="brand" href="index.html">
                        <span><strong>Gestão</strong> Portfólio</span>
                    </a>
                    <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
                </div>
                <!-- Branding end -->
                <!-- Left-side navigation -->
                <ul class="nav-left pull-left list-unstyled list-inline">
                    <li class="sidebar-collapse divided-right">
                        <a role="button" tabindex="0" class="collapse-sidebar">
                            <i class="fa fa-outdent"></i>
                        </a>
                    </li>
                    <li class="dropdown divided-right settings">
                        <a role="button" tabindex="0" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-cog"></i>
                        </a>
                        <ul class="dropdown-menu with-arrow animated littleFadeInUp" role="menu">
                            <li>
                                <ul class="color-schemes list-inline">
                                    <li class="title">Cor cabeçalho:</li>
                                    <li><a role="button" tabindex="0" class="scheme-drank header-scheme" data-scheme="scheme-default"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-black header-scheme" data-scheme="scheme-black"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-greensea header-scheme" data-scheme="scheme-greensea"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-cyan header-scheme" data-scheme="scheme-cyan"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-lightred header-scheme" data-scheme="scheme-lightred"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-light header-scheme" data-scheme="scheme-light"></a></li>
                                    <li class="title">Cor logotipo:</li>
                                    <li><a role="button" tabindex="0" class="scheme-drank branding-scheme" data-scheme="scheme-default"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-black branding-scheme" data-scheme="scheme-black"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-greensea branding-scheme" data-scheme="scheme-greensea"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-cyan branding-scheme" data-scheme="scheme-cyan"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-lightred branding-scheme" data-scheme="scheme-lightred"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-light branding-scheme" data-scheme="scheme-light"></a></li>
                                    <li class="title">Cor menu lateral:</li>
                                    <li><a role="button" tabindex="0" class="scheme-drank sidebar-scheme" data-scheme="scheme-default"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-black sidebar-scheme" data-scheme="scheme-black"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-greensea sidebar-scheme" data-scheme="scheme-greensea"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-cyan sidebar-scheme" data-scheme="scheme-cyan"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-lightred sidebar-scheme" data-scheme="scheme-lightred"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-light sidebar-scheme" data-scheme="scheme-light"></a></li>
                                    <li class="title">Cor menu ativo:</li>
                                    <li><a role="button" tabindex="0" class="scheme-drank color-scheme" data-scheme="drank-scheme-color"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-black color-scheme" data-scheme="black-scheme-color"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-greensea color-scheme" data-scheme="greensea-scheme-color"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-cyan color-scheme" data-scheme="cyan-scheme-color"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-lightred color-scheme" data-scheme="lightred-scheme-color"></a></li>
                                    <li><a role="button" tabindex="0" class="scheme-light color-scheme" data-scheme="light-scheme-color"></a></li>
                                </ul>
                            </li>
                            <li>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-xs-8 control-label">Fixar cabeçalho</label>
                                        <div class="col-xs-4 control-label">
                                            <div class="onoffswitch lightred small">
                                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="fixed-header" checked="">
                                                <label class="onoffswitch-label" for="fixed-header">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-xs-8 control-label">Fixar menu lateral</label>
                                        <div class="col-xs-4 control-label">
                                            <div class="onoffswitch lightred small">
                                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="fixed-aside" checked="">
                                                <label class="onoffswitch-label" for="fixed-aside">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Left-side navigation end -->
                <!-- Right-side navigation -->
                <ul class="nav-right pull-right list-inline">
                    <li class="dropdown nav-profile">
                        <a href class="dropdown-toggle" data-toggle="dropdown">
                            <img src="lib/images/profile-photo.jpg" alt="" class="img-circle size-30x30">
                            <span> <?php echo $usuario;?> <i class="fa fa-angle-down"></i></span>
                        </a>
                        <ul class="dropdown-menu animated littleFadeInRight" role="menu">
                            <li>
                                <a role="button" tabindex="0">
                                    <span class="label bg-blue pull-right">Texto</span>
                                    <i class="fa fa-user"></i>Usuário
                                </a>
                            </li>
                            <li>
                                <a role="button" tabindex="0">
                                    <span class="label bg-green pull-right">Texto</span>
                                    <i class="fa fa-cog"></i>Configurações
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php" role="button" tabindex="0">
                                <span class="label bg-orange pull-right">Texto</span>
                                <i class="fa fa-sign-out"></i>Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </header>
        </section>
        <!--/ HEADER Content  -->

        <!-- =================================================
        ================= CONTROLS Content ===================
        ================================================== -->
        <div id="controls">

            <!-- ================================================
            ================= SIDEBAR Content ===================
            ================================================= -->
            <aside id="sidebar">
                <div id="sidebar-wrap">
                    <div class="panel-group slim-scroll" role="tablist">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#sidebarNav">
                                        Métricas <i class="fa fa-angle-up"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">

                                    <!-- ===================================================
                                    ================= NAVIGATION Content ===================
                                    ==================================================== -->
                                    <ul id="navigation">
                                        <li class="active"><a href="index.html"><i class="fa fa-dashboard"></i> <span>Análise Coorte</span><span class="label bg-slategray label-success">Triângulo</span></a></a></li>
                                    </ul>
                                    <!--/ NAVIGATION Content -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <!--/ SIDEBAR Content -->
            <!--/ RIGHTBAR Content -->
        </div>

        <!--/ CONTROLS Content -->
        <!-- ====================================================
        ================= CONTENT ===============================
        ===================================================== -->
        <section id="content">
            <div class="page page-dashboard">
                <!-- cards row -->
                <div class="row">
                    <!-- col -->
                    <div class="card-container col-lg-4 col-sm-6 col-sm-12">
                        <div class="card">
                            <div class="front bg-greensea">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-xs-4">
                                        <i class="fa fa-users fa-4x"></i>
                                    </div>
                                    <!-- /col -->
                                    <!-- col -->
                                    <div class="col-xs-8">
                                        <p class="text-elg text-strong mb-0">86</p>
                                        <span>Churn rate</span>
                                    </div>
                                    <!-- /col -->
                                </div>
                                <!-- /row -->
                            </div>
                            <div class="back bg-greensea">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-xs-4">
                                        <a><p class="text-elg text-strong mb-0">First</p>0,9814</a>
                                    </div>
                                    <!-- /col -->
                                    <!-- col -->
                                    <div class="col-xs-4">
                                    <a><p class="text-elg text-strong mb-0">Last</p>0,9721</a>
                                    </div>
                                    <!-- /col -->
                                    <!-- col -->
                                    <div class="col-xs-4">
                                        <a href=#><i class="fa fa-ellipsis-h fa-2x"></i> Mais</a>
                                    </div>
                                    <!-- /col -->
                                </div>
                                <!-- /row -->
                            </div>
                        </div>
                    </div>
                    <!-- /col -->
                    <!-- col -->
                    <div class="card-container col-lg-4 col-sm-6 col-sm-12">
                        <div class="card">
                            <div class="front bg-lightred">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-xs-4">
                                        <i class="fa  fa-bar-chart-o fa-4x"></i>
                                    </div>
                                    <!-- /col -->
                                    <!-- col -->
                                    <div class="col-xs-8">
                                        <p class="text-elg text-strong mb-0">47%</p>
                                        <span>Slash rate</span>
                                    </div>
                                    <!-- /col -->
                                </div>
                                <!-- /row -->
                            </div>
                            <div class="back bg-lightred">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-xs-4">
                                        <a href=#><i class="fa fa-cog fa-2x"></i> Settings</a>
                                    </div>
                                    <!-- /col -->
                                    <!-- col -->
                                    <div class="col-xs-4">
                                        <a href=#><i class="fa fa-chain-broken fa-2x"></i> Drill Down</a>
                                    </div>
                                    <!-- /col -->
                                    <!-- col -->
                                    <div class="col-xs-4">
                                        <a href=#><i class="fa fa-ellipsis-h fa-2x"></i> Mais</a>
                                    </div>
                                    <!-- /col -->
                                </div>
                                <!-- /row -->
                            </div>
                        </div>
                    </div>
                    <!-- /col -->
                    <!-- col -->
                    <div class="card-container col-lg-4 col-sm-6 col-sm-12">
                        <div class="card">
                            <div class="front bg-blue">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-xs-4">
                                        <i class="fa fa-usd fa-4x"></i>
                                    </div>
                                    <!-- /col -->
                                    <!-- col -->
                                    <div class="col-xs-8">
                                        <p class="text-elg text-strong mb-0">165.000,00</p>
                                        <span>Receita Média</span>
                                    </div>
                                    <!-- /col -->
                                </div>
                                <!-- /row -->
                            </div>
                            <div class="back bg-blue">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-xs-6">
                                        <a href=#><i class="fa fa-cog fa-2x"></i> Settings</a>
                                    </div>
                                    <!-- /col -->
                                    <!-- col -->
                                    <div class="col-xs-6">
                                        <a href=#><i class="fa fa-ellipsis-h fa-2x"></i> Mais</a>
                                    </div>
                                    <!-- /col -->
                                </div>
                                <!-- /row -->
                            </div>
                        </div>
                    </div>
                    <!-- /col -->
                </div>
                <!-- Triangulo -->
                <div class="row">
                    <div class="col-md-12">
                        <div id="triangle-chart-container"></div>
                        <br>
                    </div>
                </div>
                <!-- Triangulo DrillDown -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- tile -->
                        <section class="tile">                    
                            <!-- tile header -->
                            <div class="tile-header dvd dvd-btm">
                                <h1 class="custom-font"><strong>Triângulo</strong> DrillDown</h1>
                                <ul class="controls">
                                    <li class="dropdown">
                                        <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                            <i class="fa fa-cog"></i>
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                            <li>
                                                <a role="button" tabindex="0" class="tile-toggle">
                                                    <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimizar</span>
                                                    <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expandir</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="tile-refresh">
                                                    <i class="fa fa-refresh"></i> Atualizar
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="tile-fullscreen">
                                                    <i class="fa fa-expand"></i> Tela cheia
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <table id="triangle-table-container" class="display" width="100%"></table>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        
        <!--/ CONTENT -->
    </div>
    <!--/ Application Content -->
    <!-- ============================================
    ============== Vendor JavaScripts ===============
    ============================================= -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="lib/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="lib/js/vendor/bootstrap/bootstrap.min.js"></script>
    <script src="lib/js/vendor/jRespond/jRespond.min.js"></script>
    <script src="lib/js/vendor/d3/d3.min.js"></script>
    <script src="lib/js/vendor/d3/d3.layout.min.js"></script>
    <script src="lib/js/vendor/rickshaw/rickshaw.min.js"></script>
    <script src="lib/js/vendor/sparkline/jquery.sparkline.min.js"></script>
    <script src="lib/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="lib/js/vendor/animsition/js/jquery.animsition.min.js"></script>
    <script src="lib/js/vendor/daterangepicker/moment.min.js"></script>
    <script src="lib/js/vendor/daterangepicker/daterangepicker.js"></script>
    <script src="lib/js/vendor/screenfull/screenfull.min.js"></script>
    <script src="lib/js/vendor/flot/jquery.flot.min.js"></script>
    <script src="lib/js/vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
    <script src="lib/js/vendor/flot-spline/jquery.flot.spline.min.js"></script>
    <script src="lib/js/vendor/easypiechart/jquery.easypiechart.min.js"></script>
    <script src="lib/js/vendor/raphael/raphael-min.js"></script>
    <script src="lib/js/vendor/morris/morris.min.js"></script>
    <script src="lib/js/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="lib/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="lib/js/vendor/datatables/js/jquery.dataTables.min.js"></script>
    
    <script src="lib/js/vendor/datatables/extensions/dataTables.bootstrap.js"></script>
    <script src="lib/js/vendor/chosen/chosen.jquery.min.js"></script>
    <script src="lib/js/vendor/summernote/summernote.min.js"></script>
    <script src="lib/js/vendor/coolclock/coolclock.js"></script>
    <script src="lib/js/vendor/coolclock/excanvas.js"></script>
    <script src="lib/js/vendor/footable/footable.all.min.js"></script>
    
    <!-- ===============================================
    ============== Page Specific Scripts ===============
    ================================================ -->
    <script>    
        $(window).load(function(){
            //initialize datatable
            $('#project-progress').DataTable({
              "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [ "no-sort" ] }
                ],
            });
        });
    </script>

    <!-- ============================================
    ============== Custom JavaScripts ===============
    ============================================= -->
    <script src="lib/js/main.js"></script>
    <script src="lib/js/triangulo-drilldown.js"></script>
    <script src="lib/js/triangulo.js"></script>
</body>
</html>
