<?php

	// Conexao Banco de dados
	require_once('lib/php/conexaoDb.php');

    // Classes para usuario, conexao e recuperacao de senha
    require_once('lib/php/user.php');
	require_once('lib/php/phpmailer/mail.php');
	
	// Instancia de novo usuario
    $user = new User($db);
	
	// Se estiver conectado redirecionar para área de membros
	if( $user->is_logged_in() ){ 
		header('Location: dashboard.php'); exit(); 
	}

	// Se submeteu login
	if(isset($_POST['submit'])){

		// Se nao informou o nome do usuario
		if (!isset($_POST['username'])){ 
			$error[] = "Informe o nome do usuário";
		}

		// Se nao informou a senha
		if (!isset($_POST['password'])){ 
			$error[] = "Informe a senha";
		}

		// Armazena nome do usuario
		$username = $_POST['username'];
	
		// Se o nome do usuario for valido
		if ( $user->isValidUsername($username)){
			// Se nao informou a senha
			if (!isset($_POST['password'])){
				$error[] = 'Senha deve ser informada!';
			}

			// Armazena senha
			$password = $_POST['password'];

			// Efetua login
			if($user->login($username,$password)){
				$_SESSION['username'] = $username;
				header('Location: dashboard.php');
				exit;

			} else {
				$error[] = 'Nome de usuário, senha incorreto ou sua conta não foi ativada!';
			}
		} else {
			$error[] = 'Os nomes de usuários devem ser alfanuméricos e entre 3 a 40 caracteres!';
		}
	} // Final da submissao

	// Titulo da pagina
	$title = 'Login do usuário';

	// Template cabecalho
	require_once('header.php'); 
?>
<div class="container clearfix w-3xl">
	<form role="form" method="post" action="" autocomplete="off">
		<h2><i class="fa fa-unlock"></i> Login da sua conta</h2>
		<p>Não possui cadastro? <a href='signup.php'>Cadastre-se</a></p>
		<?php
			// Verifica se possui erros
			if(isset($error)){
				foreach($error as $error){
					echo '<p class="bg-danger">'.$error.'</p>';
				}
			}
			// Se ocorreu acao
			if(isset($_GET['action'])){
				// Verifica acao
				switch ($_GET['action']) {
					case 'active':
						echo "<h4 class='alert alert-success'>Sua conta está ativa agora você pode fazer login.</h4>";
						break;
					case 'reset':
						echo "<h4 class='alert alert-success'>Verifique sua caixa de entrada e acesse o link de redefinição.</h4>";
						break;
					case 'resetAccount':
						echo "<h4 class='alert alert-success'>Senha alterada, pode agora fazer login.</h4>";
						break;
				}
			}
		?>
        <div class="row">
			<div class="form-group col-md-12">	
				<input type="text" name="username" id="username" class="form-control input-lg" placeholder="Nome do usuário" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['username'], ENT_QUOTES); } ?>" tabindex="1">
			</div>
			<div class="form-group col-md-12">
				<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Senha" tabindex="3">
			</div>
			<div class="form-group col-md-12">
				 <a href='reset.php'>Esqueceu a senha?</a>
			</div>
			<hr>
			<div class="col-md-12">
				<input type="submit" name="submit" value="Login" class="myBtn myBtn-rounded myBtn-dark m-0 mt-10" tabindex="5">
			</div>
		</div>
	</form>
</div>
<?php 
	// Template rodape
	require_once('footer.php'); 
?>