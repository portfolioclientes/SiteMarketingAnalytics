<?php  
    $zip = new ZipArchive;
    $res = $zip->open('ScriptSQL/zip/script.zip');
    if ($res === TRUE) {
        $zip->extractTo('ScriptSQL/');
        $zip->close();
        
        // Arquivo com script        
        $scriptSql = file("ScriptSQL/script.sql");
        
	    // Classe senha
    	require_once('lib/php/conexaoDb.php');
        
       	// Processa a linhas do arquivo
        foreach($scriptSql as $linha){
			// Prepara a query de insercao
			$stmt = $db->prepare($linha);
			$stmt->execute();
        }
     
        echo 'Tabela gravada com sucesso!';
        
        unlink('./ScriptSQL/script.sql');
        unlink('./ScriptSQL/zip/script.zip');
    } else {
        echo 'Nao foi enviado arquivo para importacao!';
    }
?>
