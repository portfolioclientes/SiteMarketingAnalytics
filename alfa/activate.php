<?php
	// Conexao Banco de dados
	require_once('lib/php/conexaoDb.php');

    // Classes para usuario, conexao e recuperacao de senha
    require_once('lib/php/user.php');

    // Instancia de novo usuario
    $user = new User($db);

	// Valores da url
	$memberID = trim($_GET['x']);
	$active = trim($_GET['y']);

	// Se id for number e o token ativo não estiver vazio, continue
	if(is_numeric($memberID) && !empty($active)){
		// Atualizar registro de usuários, definir a coluna ativa para "Yes", onde o memberID e o valor ativo correspondem aos fornecidos na matriz
		$stmt = $db->prepare("UPDATE members SET active = 'Yes' WHERE memberID = :memberID AND active = :active");
		$stmt->execute(array(
			':memberID' => $memberID,
			':active' => $active
		));
		// Se a linha foi atualizada, redirecione o usuário
		if($stmt->rowCount() == 1){
			// Redireciona para pagina de login
			header('Location: login.php?action=active');
			exit;
		} else {
			echo "Sua conta não foi ativada!"; 
		}
	}
?>