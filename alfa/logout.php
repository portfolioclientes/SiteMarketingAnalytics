<?php 
	// Conexao Banco de dados
	require_once('lib/php/conexaoDb.php');

    // Classes para usuario, conexao e recuperacao de senha
    require_once('lib/php/user.php');
	
	// Instancia de novo usuario
    $user = new User($db);

    // LogOut
    $user->logout(); 

    // Retorna para pagina inicial
    header('Location: index.html');

    exit;
?>