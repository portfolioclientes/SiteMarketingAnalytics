create database if not exists u680651490_brots DEFAULT charset=utf8 collate=utf8_unicode_ci;

use u680651490_brots;

--
-- Cria tabela de usuarios
--
drop table if exists members;
create table if not exists members (
  `memberID`                   int(11) NOT NULL AUTO_INCREMENT,
  `username`                   varchar(255) NOT NULL,
  `password`                   varchar(255) NOT NULL,
  `email`                      varchar(255) NOT NULL,
  `active`                     varchar(255) NOT NULL,
  `resetToken`                 varchar(255) DEFAULT NULL,
  `resetComplete`              varchar(3)  DEFAULT 'No',
    primary key(memberID),
    unique (username)
) engine=InnoDB charset=utf8 collate=utf8_general_ci comment='Cadastro de usuarios';

--
-- Cria tabela de Historico mensal de clientes
--
drop table if exists historico_mensal_clientes;
create table historico_mensal_clientes (
  `Id`                         int(11) NOT NULL AUTO_INCREMENT,
  `Cli`                        int(11) NOT NULL,
  `Data`                       date NOT NULL,
  `Per`                        int(3) NOT NULL,
  `Mc`                         int(11) DEFAULT NULL,
  `McCorrig`                   int(11) DEFAULT NULL,
  `Ativo`                      tinyint(1) DEFAULT NULL,
  `Ciclo`                      int(3) DEFAULT NULL,
  `Coorte`                     int(3) DEFAULT NULL,
    primary key(Id)
) engine=InnoDB charset=utf8 collate=utf8_general_ci comment='Historico mensal de clientes';

--
-- Cria tabela de Key Performance Indicator Gerais
--
drop table if exists kpi_gerais;
create table kpi_gerais (
   `Id`                        int not null AUTO_INCREMENT, 
   `Per`                       int(4) not null,
   `QtdCliAtivos`              int default null,
   `McMedia`                   decimal(10,2) default null, 
   `QtdReaquisicoes`           int default null,
   `McSd`                      decimal(10,2) default null, 
   `McVar`                     decimal(10,2)  default null, 
   `QtdAquisicoesBruta`        int default null, 
   `QtdAquisicoesLiq`          int default null,
   `QtdDesercoes`              int default null,
   `TaxasAquisicaoBruta`       decimal(19,18) default null,
   `TaxaAquisicoesLiq`         decimal(19,18) default null,
   `TaxasReaquisicaoBase`      decimal(19,18) default null,
   `TaxasReaquisicaoAquisicao` decimal(19,18) default null,
   `TaxasRetencao`             decimal(19,18) default null,
   `TaxasDesercao`             decimal(19,18) default null, 
   `TaxasCrescimentoBaseCli`   decimal(19,18) default null, 
   `ClvTopDown`                decimal(19,18) default null,
     primary key(Id)
) engine=InnoDB charset=utf8 collate=utf8_general_ci comment='Key Performance Indicator Gerais';

--
-- Cria tabela de Key Performance Indicator Coortes 
--
drop table if exists `kpi_coortes`;
create table  `kpi_coorte` (
  `Id`                        int(11) NOT NULL AUTO_INCREMENT,
  `Coorte`                    int(11) NOT NULL,
  `Per`                       int(11) NOT NULL,
  `Cli`                       int(11) NOT NULL,
     primary key(Id)
) engine=InnoDB charset=utf8 collate=utf8_general_ci comment='Key Performance Indicator Coortes';

--
-- Cria tabela Key Performance Indicator dos Coortes para Triangulo
--
drop table if exists kpi_coortes_triangulos;

create table kpi_coortes_triangulos (
  `Id`                        int(11) DEFAULT NULL,
  `Coorte`                    int(11) DEFAULT NULL,
  `Per`                       int(11) DEFAULT NULL,
  `QtdCliAtivos`              int(11) DEFAULT NULL,
  `PercCliAtivosLast`         decimal(19,18) default null,
  `PercCliAtivosFirst`        decimal(19,18) default null,
) engine = InnoDB charset=utf8 collate=utf8_general_ci comment='Key Performance Indicator dos Coortes para Triangulo';

--
-- Cria tabela de Key Performance Indicator de Recencia e Frequencia
--
drop table if exists kpi_recencias_frequencias;
create table kpi_recencias_frequencias (
   `Id`                        int not null  AUTO_INCREMENT, 
   `Freq`                      int(4) not null,
   `Rec`                       int(4) not null,
   `Cli`                       int not null,
   `Coorte`                    int not null,
     primary key(Id)
) engine = InnoDB charset=utf8 collate=utf8_general_ci comment='Key Performance Indicator de Recencia e Frequencia';

