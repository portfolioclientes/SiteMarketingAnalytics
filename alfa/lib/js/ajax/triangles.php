<?php
    include "config.php";

    $sql =
        "SELECT
            Coorte,
            Per,
            QtdCliAtivos
        FROM coorte_triangulos
        ORDER BY
            Coorte,
            Per";

    $stmt = $db->prepare($sql);
    $stmt->execute();

    $data = array();
    while ($row = $stmt->fetch()) {
        $data[] = array(
            'row' => $row['Coorte'],
            'col' => $row['Per'],
            'value' => $row['QtdCliAtivos']
        );
    }

    echo json_encode($data);
?>