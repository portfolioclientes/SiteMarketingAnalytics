<?php
include "../includes/database.php";

$sql =
    "SELECT
        Cli
    FROM kpicoorte
    WHERE
        Coorte = :coorte
        AND Per = :per";

$stmt = $db->prepare($sql);
$stmt->execute(array(':coorte' => $_REQUEST['coorte'], ':per' => $_REQUEST['per']));

$data = array();
while ($row = $stmt->fetch()) {
    $data[] = $row['Cli'];
}

echo json_encode($data);