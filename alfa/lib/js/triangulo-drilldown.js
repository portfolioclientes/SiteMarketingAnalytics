// Cria tabela DrillDown de clientes do Triangulo
function createTriangleDrillDown($container, dataTriangle) {

  $container.empty()

  $('#triangle-table-container').DataTable( {
    
    "language": {
      "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
    },

    destroy: true,
    
    data: dataTriangle,

    columns: [
      { title: "Cliente" }
    ]
  });
};
