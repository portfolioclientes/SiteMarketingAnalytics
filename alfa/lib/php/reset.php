<?php 
	// Conexao Banco de dados
	require_once('lib/php/conexaoDb.php');

    // Classes para usuario, conexao e recuperacao de senha
    require_once('lib/php/user.php');

    // Instancia de novo usuario
    $user = new User($db);


	// Se conectado redirecionar para área de membros
	if( $user->is_logged_in() ){ header('Location: dashboard.php'); exit(); }

	// Se o formulário foi submetido processá-lo
	if(isset($_POST['submit'])){

		// Verifique se todos os campos foram declarados
		if (!isset($_POST['email'])) $error[] = "Preencha todos os campos";

		// E-mail de validacao
		if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
			$error[] = 'Informe um e-mail válido!';
		} else {
			$stmt = $db->prepare('SELECT email FROM members WHERE email = :email');
			$stmt->execute(array(':email' => $_POST['email']));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			if(empty($row['email'])){
				$error[] = 'E-mail fornecido não é reconhecido!';
			}
		}

		// Se nao encontrou erros na validacao
		if(!isset($error)){

			// Cria um codigo de ativacao
			$stmt = $db->prepare('SELECT password, email FROM members WHERE email = :email');
			$stmt->execute(array(':email' => $_POST['email']));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			// Hash, Key e random data
			$token = hash_hmac('SHA256', $user->generate_entropy(8), $row['password']);

			// Hash e Key armazenada no banco de dados, o valor normal é enviado ao usuário
			$storedToken = hash('SHA256', ($token));

			try {

				$stmt = $db->prepare("UPDATE members SET resetToken = :token, resetComplete='No' WHERE email = :email");
				$stmt->execute(array(
					':email' => $row['email'],
					':token' => $storedToken
				));

				//send email
				$to = $row['email'];
				$subject = "Resetar a senha";
				$body = "<p>Alguém pediu que a senha fosse reiniciada.</p>
				<p>Se isso foi um erro, ignore este e-mail e nada acontecerá.</p>
				<p>Para redefinir sua senha, visite o seguinte endereço:<a href='".DIR."resetPassword.php?key=$token'>".DIR."resetPassword.php?key=$token</a></p>";

				$mail = new Mail();
				$mail->setFrom(SITEEMAIL);
				$mail->addAddress($to);
				$mail->subject($subject);
				$mail->body($body);
				$mail->send();

				//redirect to index page
				header('Location: login.php?action=reset');
				exit;

			//else catch the exception and show the error.
			} catch(PDOException $e) {
				$error[] = $e->getMessage();
			}
		}
	}

	// Titulo da pagina
	$title = 'Redefinir conta';

	// Template de cabecalho
	require('header.php');
?>
<div class="container">
	<div class="row">
	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" method="post" action="" autocomplete="off">
				<h2>Redefinir Senha</h2>
				<hr>
				<?php
					// Verifica se possui erros
					if(isset($error)){
						foreach($error as $error){
							echo '<p class="bg-danger">'.$error.'</p>';
						}
					}
					if(isset($_GET['action'])){
						// Verifica a acao
						switch ($_GET['action']) {
							case 'active':
								echo "<h2 class='bg-success'>Sua conta foi ativada.</h2>";
								break;
							case 'reset':
								echo "<h2 class='bg-success'>Verifique seu e-mail para link de redefinição da senha.</h2>";
								break;
						}
					}
				?>
				<div class="form-group">
					<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" value="" tabindex="1">
				</div>
				<hr>
				<div class="row">
					<div class="col-xs-6 col-md-6"><input type="submit" name="submit" value="Enviar link para redefinir senha" class="btn btn-primary btn-block btn-lg" tabindex="2"></div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
	// Adiciona template de rodape
	require('footer.php');
?>