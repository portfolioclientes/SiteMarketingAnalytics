<?php
	// Conexao Banco de dados
	require_once('lib/php/conexaoDb.php');

    // Classes para usuario, conexao e recuperacao de senha
    require_once('lib/php/user.php');

    // Instancia de novo usuario
    $user = new User($db);


	// Se conectado redirecionar para a área de membros
	if( $user->is_logged_in() ){ header('Location: dashboard.php'); exit(); }

	$resetToken = hash('SHA256', ($_GET['key']));

	$stmt = $db->prepare('SELECT resetToken, resetComplete FROM members WHERE resetToken = :token');
	$stmt->execute(array(':token' => $resetToken));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	// Se não for token do db, mate a página
	if(empty($row['resetToken'])){
		$stop = 'Token inválido fornecido, use o link fornecido no e-mail de reinicialização.';
	} elseif($row['resetComplete'] == 'Yes') {
		$stop = 'Sua senha já foi alterada!';
	}

	// Se o formulário foi submetido processá-lo
	if(isset($_POST['submit'])){

		if (!isset($_POST['password']) || !isset($_POST['passwordConfirm']))
			$error[] = 'Ambos os campos da senha devem ser inseridos!';

		// Validacao basica
		if(strlen($_POST['password']) < 3){
			$error[] = 'Senha deve ter mais de três caracteres!';
		}

		if(strlen($_POST['passwordConfirm']) < 3){
			$error[] = 'Confirmação da senha possui menos que três caracteres!';
		}

		if($_POST['password'] != $_POST['passwordConfirm']){
			$error[] = 'Senhas não coincidem!';
		}

		// Se nao possui erros
		if(!isset($error)){

			// Gera hash da senha
			$hashedpassword = $user->password_hash($_POST['password'], PASSWORD_BCRYPT);

			try {

				$stmt = $db->prepare("UPDATE members SET password = :hashedpassword, resetComplete = 'Yes'  WHERE resetToken = :token");
				$stmt->execute(array(
					':hashedpassword' => $hashedpassword,
					':token' => $row['resetToken']
				));

				// Redireciona para pagina de redefinicao da senha
				header('Location: login.php?action=resetAccount');
				exit;

			// Mostra excecao
			} catch(PDOException $e) {
				$error[] = $e->getMessage();
			}

		}

	}

	// Titulo
	$title = 'Redefinir conta';

	// Template de cabecalho
	require('header.php'); 
?>
<div class="container">
	<div class="row">
	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
	    	<?php if(isset($stop)){
	    		echo "<p class='bg-danger'>$stop</p>";
	    	} else { ?>
				<form role="form" method="post" action="" autocomplete="off">
					<h2>Mudar senha</h2>
					<hr>
					<?php
						// Verifica se possui erros
						if(isset($error)){
							foreach($error as $error){
								echo '<p class="bg-danger">'.$error.'</p>';
							}
						}
						// Verifica acao
						switch ($_GET['action']) {
							case 'active':
								echo "<h2 class='bg-success'>Sua conta foi ativada!</h2>";
								break;
							case 'reset':
								echo "<h2 class='bg-success'>Verifique seu e-mail para link de redefinição da senha.</h2>";
								break;
						}
					?>
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Senha" tabindex="1">
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="password" name="passwordConfirm" id="passwordConfirm" class="form-control input-lg" placeholder="Confirmação da senha" tabindex="1">
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-xs-6 col-md-6"><input type="submit" name="submit" value="Alterar senha" class="btn btn-primary btn-block btn-lg" tabindex="3"></div>
					</div>
				</form>
			<?php } ?>
		</div>
	</div>
</div>
<?php 
	// Template de rodape
	require('footer.php'); 
?>