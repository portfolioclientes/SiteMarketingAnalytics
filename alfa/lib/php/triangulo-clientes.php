<?php
	// Conexao Banco de dados
    include('conexaoDb.php');

    $sql =
        "SELECT
            Cli
        FROM kpi_coortes
        WHERE
            Coorte = :coorte AND
               Per = :per
        ORDER BY
            Cli 
       ";

    $stmt = $db->prepare($sql);
    $stmt->execute(array(':coorte' => $_REQUEST['coorte'], ':per' => $_REQUEST['per']));

    $dataTriangle = array();
    while ($row = $stmt->fetch()) {
        $dataTriangle[] = $row;
    }

    echo json_encode($dataTriangle);
?>