<?php
    // Elimina variaveis da sessao
    $_SESSION=array();
    unset($num);
    
    // Elimina sessao
    session_destroy();

    // Inicializa com a sessao nao disponivel para usuario
    $usuariOk = FALSE;

    // Retorna para pagina inicial
    header("Location:DashBoard.php");
?>